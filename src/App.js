import React, { Component } from 'react'
import Navbar from './components/Navbar';
import './App.css';

import Counters from './components/Counters'

class App extends Component {

  state = {
    id: 0,
    counters: [],
  };
  handleCounter = () => {
    const counters = [...this.state.counters]
    const id = this.state.id + 1
    this.setState({ id })
    counters.push({ id, value: 0 })
    this.setState({ counters })
  }
  handleDelete = (counterId) => {
    const counters = this.state.counters.filter(
      (counter) => counter.id !== counterId
    );
    this.setState({ counters });
    // console.log("Event Handler Callled", counterId);
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };
  handleDecrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    if (counters[index].value > 0) {
      counters[index].value--;
    }
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((counter) => {
      counter.value = 0;
      return counter;
    });
    this.setState({ counters });
  };


  render() {

    return (
      <React.Fragment>
        <Navbar
          totalCounters={this.state.counters.reduce((accu, counter) => {
            accu += counter.value
            return accu
          }, 0)} />

        <main className="container">
          <Counters
            addCounter={this.handleCounter}
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
            onDelete={this.handleDelete} />
        </main>

      </React.Fragment >

    );
  }
}

export default App;
